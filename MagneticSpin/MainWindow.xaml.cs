﻿using MagneticSpin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MagneticSpin
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const UInt16 LeftTopX = 20;
        const UInt16 LeftTopY = 20;
        const UInt16 RightBottomX = 780;
        const UInt16 RightBottomY = 580;

        SpinAnimation animation;
        DispatcherTimer timer;
        DispatcherTimer timer_graph;
        bool bSpinsPlotted = false;

        public MainWindow()
        {
            InitializeComponent();
            DrawGrid();

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(50);
            timer.Tick += DrawingThread;

            timer_graph = new DispatcherTimer();
            timer_graph.Interval = TimeSpan.FromMilliseconds(50);
            timer_graph.Tick += DrawingThreadGraph;
        }

        private void buttonPlotSpins_Click(object sender, RoutedEventArgs e)
        {
            if (!bSpinsPlotted)
            {
                animation = new SpinAnimation(double.Parse(textBoxSpinProbability.Text, System.Globalization.CultureInfo.InvariantCulture),
                    double.Parse(textBoxTemperature.Text, System.Globalization.CultureInfo.InvariantCulture));
                DrawSpins();
                bSpinsPlotted = true;
            }
        }

        private void buttonStartSimulation_Click(object sender, RoutedEventArgs e)
        {
            if (bSpinsPlotted)
            {
                buttonStartSimulation.IsEnabled = false;
                buttonPlotParticles.IsEnabled = false;
                buttonGenerateGraphs.IsEnabled = false;
                timer.Start();
            }
        }

        private void DrawGrid()
        {
            for (int index_columns = LeftTopX; index_columns <= RightBottomX; index_columns += 20)
            {
                Line objLine = new Line();
                objLine.Stroke = System.Windows.Media.Brushes.Black;
                objLine.Fill = System.Windows.Media.Brushes.Black;
                objLine.X1 = index_columns;
                objLine.Y1 = LeftTopY;
                objLine.X2 = index_columns;
                objLine.Y2 = RightBottomY;
                MainCanvas.Children.Add(objLine);
            }
            for (int index_lines = LeftTopX; index_lines <= RightBottomY; index_lines += 20)
            {
                Line objLine = new Line();
                objLine.Stroke = System.Windows.Media.Brushes.Black;
                objLine.Fill = System.Windows.Media.Brushes.Black;
                objLine.X1 = LeftTopX;
                objLine.Y1 = index_lines;
                objLine.X2 = RightBottomX;
                objLine.Y2 = index_lines;
                MainCanvas.Children.Add(objLine);
            }
        }

        private void DrawSpins()
        {
            MainCanvas.Children.Clear();
            if (!bSpinsPlotted)
            {
                animation.CreateSpinList();
            }
            DrawGrid();
            foreach (Spin spin in animation.spinList)
            {
                Line objLine_1 = new Line();
                Line objLine_2 = new Line();
                Line objLine_3 = new Line();

                if (spin.spinOrientation == 1)
                {
                    objLine_1.Stroke = System.Windows.Media.Brushes.Black;
                    objLine_1.Fill = System.Windows.Media.Brushes.Black;
                    objLine_1.X1 = spin.spinCoords.X + LeftTopX / 2;
                    objLine_1.Y1 = spin.spinCoords.Y + LeftTopY - 2;
                    objLine_1.X2 = spin.spinCoords.X + LeftTopX / 2;
                    objLine_1.Y2 = spin.spinCoords.Y + 2;

                    objLine_2.Stroke = System.Windows.Media.Brushes.Black;
                    objLine_2.Fill = System.Windows.Media.Brushes.Black;
                    objLine_2.X1 = spin.spinCoords.X + LeftTopX / 2;
                    objLine_2.Y1 = spin.spinCoords.Y + 2;
                    objLine_2.X2 = spin.spinCoords.X + LeftTopX / 2 + 5;
                    objLine_2.Y2 = spin.spinCoords.Y + 7 ;

                    objLine_3.Stroke = System.Windows.Media.Brushes.Black;
                    objLine_3.Fill = System.Windows.Media.Brushes.Black;
                    objLine_3.X1 = spin.spinCoords.X + LeftTopX / 2;
                    objLine_3.Y1 = spin.spinCoords.Y + 2;
                    objLine_3.X2 = spin.spinCoords.X + LeftTopX / 2 - 5;
                    objLine_3.Y2 = spin.spinCoords.Y + 7 ;

                    MainCanvas.Children.Add(objLine_1);
                    MainCanvas.Children.Add(objLine_2);
                    MainCanvas.Children.Add(objLine_3);
                }
                else
                {
                    objLine_1.Stroke = System.Windows.Media.Brushes.Red;
                    objLine_1.Fill = System.Windows.Media.Brushes.Red;
                    objLine_1.X1 = spin.spinCoords.X + LeftTopX / 2;
                    objLine_1.Y1 = spin.spinCoords.Y + LeftTopY - 2;
                    objLine_1.X2 = spin.spinCoords.X + LeftTopX / 2;
                    objLine_1.Y2 = spin.spinCoords.Y + 2;

                    objLine_2.Stroke = System.Windows.Media.Brushes.Red;
                    objLine_2.Fill = System.Windows.Media.Brushes.Red;
                    objLine_2.X1 = spin.spinCoords.X + LeftTopX / 2;
                    objLine_2.Y1 = spin.spinCoords.Y + LeftTopY - 2;
                    objLine_2.X2 = spin.spinCoords.X + LeftTopX / 2 + 5;
                    objLine_2.Y2 = spin.spinCoords.Y + LeftTopY - 7;

                    objLine_3.Stroke = System.Windows.Media.Brushes.Red;
                    objLine_3.Fill = System.Windows.Media.Brushes.Red;
                    objLine_3.X1 = spin.spinCoords.X + LeftTopX / 2;
                    objLine_3.Y1 = spin.spinCoords.Y + LeftTopY - 2;
                    objLine_3.X2 = spin.spinCoords.X + LeftTopX / 2 - 5;
                    objLine_3.Y2 = spin.spinCoords.Y + LeftTopY - 7;

                    MainCanvas.Children.Add(objLine_1);
                    MainCanvas.Children.Add(objLine_2);
                    MainCanvas.Children.Add(objLine_3);
                }
            }
        }

        private void DrawingThread(object sender, EventArgs e)
        {
            try
            {
                animation.UpdateSpinOrientation();
                DrawSpins();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception tickThread : " + ex.Message);
            }
        }

        private void DrawingThreadGraph(object sender, EventArgs e)
        {
            try
            {
                animation.UpdateSpinOrientationGraph();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception tickThread : " + ex.Message);
            }
        }

        private void buttonGenerateGraphs_Click(object sender, RoutedEventArgs e)
        {
            if (bSpinsPlotted)
            {
                buttonStartSimulation.IsEnabled = false;
                buttonPlotParticles.IsEnabled = false;
                buttonGenerateGraphs.IsEnabled = false;

                timer_graph.Start();
            }
        }
    }
}
