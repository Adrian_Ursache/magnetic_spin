﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MagneticSpin.Model
{
    class Spin
    {
        public Point spinCoords { get; }

        // Up = true, Down = false
        public int spinOrientation { get; set; }

        public Spin(Point spinCoords, int spinOrientation)
        {
            this.spinCoords = spinCoords;
            this.spinOrientation = spinOrientation;
        }
    }
}
