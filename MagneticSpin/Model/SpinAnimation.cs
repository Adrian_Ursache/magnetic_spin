﻿using MagneticSpin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MagneticSpin
{
    class SpinAnimation
    {
        const UInt16 LeftTopX = 20;
        const UInt16 LeftTopY = 20;
        const UInt16 RightBottomX = 780;
        const UInt16 RightBottomY = 580;
        const double MaxVarTemp = 30;
        const int cycles = 1000;
        const UInt16 NrOfElementsInLine = 38;

        const double coefE = 2;

        public List<Spin> spinList = new List<Spin>();

        private double spinProbability;

        private double temperature;

        public SpinAnimation(double spinProbability, double temperature)
        {
            this.spinProbability = spinProbability;
            this.temperature = temperature;
        }

        public void UpdateSpinOrientation()
        {
            if (spinList.Count() > 0)
            {
                List<Spin> newSpinList = new List<Spin>();
                for (int spin_index = 0; spin_index < spinList.Count(); ++spin_index)
                {
                    double energy = 0;
                    double new_energy = 0;
                    List<int> neighbours = new List<int>();

                    neighbours.Add(spinList.ElementAt(spin_index).spinOrientation);
                    if (spin_index - NrOfElementsInLine > 0)
                    {
                        neighbours.Add(spinList.ElementAt(spin_index - NrOfElementsInLine).spinOrientation);
                    }
                    if (spin_index + NrOfElementsInLine < spinList.Count())
                    {
                        neighbours.Add(spinList.ElementAt(spin_index + NrOfElementsInLine).spinOrientation);
                    }
                    if (spin_index % NrOfElementsInLine < NrOfElementsInLine - 1)
                    {
                        neighbours.Add(spinList.ElementAt(spin_index + 1).spinOrientation);
                    }
                    if (spin_index % NrOfElementsInLine > 0)
                    {
                        neighbours.Add(spinList.ElementAt(spin_index - 1).spinOrientation);
                    }

                    for (int temp_index = 0; temp_index < neighbours.Count() - 1; ++temp_index)
                    {
                        for (int temp_index_2 = temp_index + 1; temp_index_2 < neighbours.Count(); ++temp_index_2)
                        {
                            energy += neighbours.ElementAt(temp_index) * neighbours.ElementAt(temp_index_2);
                            if (temp_index == 0)
                            {
                                new_energy += -1 * neighbours.ElementAt(temp_index) * neighbours.ElementAt(temp_index_2);
                            }
                            else
                            {
                                new_energy += neighbours.ElementAt(temp_index) * neighbours.ElementAt(temp_index_2);
                            }
                        }
                    }

                    energy *= -coefE;
                    new_energy *= -coefE;

                    newSpinList.Add(spinList.ElementAt(spin_index));

                    if (new_energy - energy > 0)
                    {
                        Random random = new Random();
                        if (random.NextDouble() < Math.Exp(-(new_energy - energy) / temperature))
                        {
                            newSpinList.ElementAt(newSpinList.Count() - 1).spinOrientation *= -1;
                        }
                    }
                    else
                    {
                        newSpinList.ElementAt(newSpinList.Count() - 1).spinOrientation *= -1;
                    }
                }
                spinList.Clear();
                spinList = newSpinList;
            }
        }

        public void UpdateSpinOrientationGraph()
        {
            if (spinList.Count() > 0)
            {
                List<Spin> newSpinList = new List<Spin>();
                for (int spin_index = 0; spin_index < spinList.Count(); ++spin_index)
                {
                    double energy = 0;
                    double new_energy = 0;
                    List<int> neighbours = new List<int>();

                    neighbours.Add(spinList.ElementAt(spin_index).spinOrientation);
                    if (spin_index - NrOfElementsInLine > 0)
                    {
                        neighbours.Add(spinList.ElementAt(spin_index - NrOfElementsInLine).spinOrientation);
                    }
                    if (spin_index + NrOfElementsInLine < spinList.Count())
                    {
                        neighbours.Add(spinList.ElementAt(spin_index + NrOfElementsInLine).spinOrientation);
                    }
                    if (spin_index % NrOfElementsInLine < NrOfElementsInLine - 1)
                    {
                        neighbours.Add(spinList.ElementAt(spin_index + 1).spinOrientation);
                    }
                    if (spin_index % NrOfElementsInLine > 0)
                    {
                        neighbours.Add(spinList.ElementAt(spin_index - 1).spinOrientation);
                    }

                    for (int temp_index = 1; temp_index < neighbours.Count() - 1; ++temp_index)
                    {
                        energy += neighbours.ElementAt(temp_index) * neighbours.ElementAt(0);
                        new_energy += -1 * neighbours.ElementAt(0) * neighbours.ElementAt(temp_index);
                    }

                    energy *= -coefE;
                    new_energy *= -coefE;

                    newSpinList.Add(spinList.ElementAt(spin_index));

                    if (new_energy - energy > 0)
                    {
                        Random random = new Random();
                        if (random.NextDouble() < Math.Exp(-(new_energy - energy) / temperature))
                        {
                            newSpinList.ElementAt(newSpinList.Count() - 1).spinOrientation *= -1;
                        }
                    }
                    else
                    {
                        newSpinList.ElementAt(newSpinList.Count() - 1).spinOrientation *= -1;
                    }
                }

                spinList.Clear();
                spinList = newSpinList;
            }
        }

        public void CreateSpinList()
        {
            spinList.Clear();

            Random random_spin = new Random();
            for (int index_columns = LeftTopX; index_columns < RightBottomX; index_columns += 20)
            {
                for (int index_lines = LeftTopX; index_lines < RightBottomY; index_lines += 20)
                {
                    int spinDirection;
                    if (random_spin.NextDouble() < spinProbability)
                    {
                        spinDirection = 1;
                    }
                    else
                    {
                        spinDirection = -1;
                    }

                    spinList.Add(new Spin(new Point(index_columns, index_lines), spinDirection));
                }
            }
        }
    }
}
